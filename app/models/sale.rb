class Sale
  include Mongoid::Document 
  include Mongoid::Timestamps
  field :confirmation_code, type: String
  field :uid, type: String
  field :month, type: Integer
  belongs_to :deal

 MONTHS = [1,2,3,4,5,6,7,8,9,10,11,12]
  after_create:add_month


  protected
  def add_month
    self.update_attributes(:month => Sale::MONTHS.sample)
  end








end
