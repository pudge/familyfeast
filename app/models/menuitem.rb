class Menuitem
  include Mongoid::Document
  field :name, type: String
  field :size, type: String
  field :price, type: String
  belongs_to :owner
end
