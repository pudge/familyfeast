class Deal
  include Mongoid::Document
  include Mongoid::Timestamps
  field :type, type: String
  field :postcode, type: String
  field :price, type: String
  field :location, type: String
  field :launched, type: Integer
  field :name, type: String
  field :restaurant_name, type: String
  field :home, type: Integer
  field :pick, type: Integer
  field :table, type: Integer
  field :month, type: Integer
  
  MONTHS = [1,2,3,4,5,6,7,8,9,10,11,12]
  after_create:add_month


  protected
  def add_month
    self.update_attributes(:month => Deal::MONTHS.sample)
  end
  
  
  
  
  
  
  
  
  
  
  
  
  belongs_to :owner
  belongs_to :user
  embeds_many :menuitems
  has_many :sales
end
