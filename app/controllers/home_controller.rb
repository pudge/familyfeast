class HomeController < ApplicationController
 before_filter :authenticate_user!, :except => [:browseresdeals, :browseresitems, :browserestaurants, :restaurants, :index, :showdeals, :userdeals]

 def restaurants

 end

 def browseresdeals
  if params[:get]
    @id=params[:get].keys[0]
    session[:uid]=@id
    @dealowner= Owner.find(@id)
    @resdeals= @dealowner.deals
  end
 end

 def browseresitems
  if params[:get1]
    @id=params[:get1].keys[0]
    session[:uid]=@id
    @dealowner= Owner.find(@id)
    @resitems = @dealowner.menuitems
  end
 end
 
 def browserestaurants
    if params[:postcode] && params[:type]
      @filter = Owner.where :postcode => params[:postcode], :type => params[:type]
    end
    if params[:type] && params[:type] != ""
      @filter = Owner.where :type => params[:type]
    end
    if params[:postcode] && params[:postcode] != ""
      @filter = Owner.where :postcode => params[:postcode]
    end

    if !params[:postcode] && !params[:type] || params[:postcode] == "" && params[:type] == ""
      @filter = Owner.all
    end


 end
 def showdeals

    if params[:postcode] && params[:type]
      @filter = Deal.where :postcode => params[:postcode], :type => params[:type], :launched=> 1
    end
    if params[:type] && params[:type] != ""
      @filter = Deal.where :type => params[:type], :launched=> 1
    end
    if params[:postcode] && params[:postcode] != ""
      @filter = Deal.where :postcode => params[:postcode], :launched=>1
    end

    if !params[:postcode] && !params[:type] || params[:postcode] == "" && params[:type] == ""
      @filter = Deal.where :launched=> 1
    end

 end



  def index

  end

  def userdeals
  @user= current_user
    if params[:get]
    @id=params[:get].keys[0]
    session[:uid]=@id
    @dealorder= Deal.find(@id)
    end
   if user_signed_in?
   @con= SecureRandom.hex(4).upcase
   @dealorder.sales.create! :confirmation_code => @con, :uid => @user.id
   end
  end

 def order
    @user= current_user
    @userdeals= @user.deals
    @userdeals << Deal.find(session[:uid])
    @user.update_attributes(:deals => @userdeals)

 end


end
