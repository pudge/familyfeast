class DealsController < ApplicationController
  # GET /deals
  # GET /deals.json
 before_filter :authenticate_owner!
  def index
    @owner= current_owner
    @deals = @owner.deals.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @deals }
    end
  end

  # GET /deals/1
  # GET /deals/1.json
  def show
    @deal = Deal.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @deal }
    end
  end

  # GET /deals/new
  # GET /deals/new.json
  def new
    @owner=current_owner
    @menuitems = @owner.menuitems.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @deal }
    end
  end

  # GET /deals/1/edit
  def edit
    @owner= current_owner
    @menuitems= @owner.menuitems.all
    @deal = Deal.find(params[:id])
   if params[:mid]
     @deal.menuitems.where(:id=>params[:mid]).delete

   respond_to do |format|
   format.html {}
    end
   end

  end
  # PUT /deals/1
  # PUT /deals/1.json
  def update
    @deal = Deal.find(params[:id])

    respond_to do |format|
      if @deal.update_attributes(params[:deal])
        format.html { redirect_to @deal, notice: 'Deal was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @deal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deals/1
  # DELETE /deals/1.json
  def destroy
    @deal = Deal.find(params[:id])
    @deal.destroy

      format.json { head :no_content }
  end


  def mid_edit
    if params[:createdeal]

         @deal= Deal.find(params[:did])
         @dealmenu = @deal.menuitems

          params[:createdeal].each do |id|

              @dealmenu << Menuitem.find(id)

           end

         @deal.update_attributes(:price => params[:price], :menuitems => @dealmenu)
    end

    respond_to do |format|
      format.html { }
     end

  end



  def create
   @owner= current_owner
   if params[:options]
    params[:options].each do |opt|
      if opt == "table"
        @table=1
      end
      if opt == "pick"
        @opt=1
      end
      if opt == "home"
        @home=1
      end
    end
   end
   @dealnew= @owner.deals.new(:restaurant_name=>@owner.restaurant_name, :home=>@home, :pick=>@pick, :table=>@table, :location=>@owner.location , :type=> @owner.type, :postcode=> @owner.postcode, :price=>params[:price], :name=>params[:name])
     @dealitems= @dealnew.menuitems
     params["createdeal"].each do |id|
      @dealitems <<  Menuitem.find(id)
     end

     id = @dealnew.id
     @dealnew.save
     session[:newdeal]= @dealnew._id
     respond_to do |format|
      format.html {render json: {'id'=>id} }
     end

  end

  def launch
   respond_to do |format|
     format.html { }
   end
  end

  def launched
   @id= params[:launch].keys[0]
   @deal= Deal.find(@id) 
   @deal.update_attributes(:launched => 1)
  end

  def salestats
    @owner= current_owner
    @dealowner= @owner.deals.all
  end










end
