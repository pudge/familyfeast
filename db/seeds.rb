# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
#
Owner.delete_all
Menuitem.delete_all
Deal.delete_all
User.delete_all
Sale.delete_all
@opt=[0,1]
@owner1= Owner.create! :about=>"finger lickin good", :address => "2 Henry Lawson Ave McMahons Point, Sydney, Australia", :restaurant_name=> "kfc", :mobile_number=>"03211111111", :email=>"italian@familyfeast.com", :password=>"dealchef", :password_confirmation=>"dealchef", :location=>"islamabad", :type=>"italian", :postcode=>"400"

@owner2= Owner.create! :about=>"over-rated", :address => "0/6 Cowper Wharf Roadway Woolloomooloo, Sydney, Australia", :restaurant_name=>"hardees", :mobile_number=>"03451111111", :email=>"thai@familyfeast.com", :password=>"dealchef", :password_confirmation=>"dealchef", :location=>"lahore", :type=>"thai", :postcode=>"4000"

@owner3= Owner.create! :about=>"m lovin it", :address => "16 Argyle StThe Rocks, Sydney, Australia", :restaurant_name=>"mcdonalds", :mobile_number=>"03111111111", :email=>"desi@familyfeast.com", :password=>"dealchef", :password_confirmation=>"dealchef", :location=>"pindi", :type=>"desi", :postcode=>"40"


 SecureRandom.hex(4).upcase
@name=["salad","pizza","pasta","rice","burger","fries"]
@price=[15,14,12,17,21,8,5,9,11]
@size=["small","medium","large"]
@dname=["deal1", "deal2", "deal3", "deal4", "deal5", "deal6", "deal7"]
@dprice=["44","33","22","66","13","15","31"]
@launched=[nil,1]
items = [ ]


Owner.all.each do |owner| 
  3.times do 
    menuitems = []
    11.times do 
      items = Menuitem.create! :name => @name.sample, :size => @size.sample, :price => @price.sample, :owner => owner
      menuitems << items
    end
    menuitems = menuitems.sample(3)
    price = menuitems.collect{|m| m.price.to_i}.reduce(:+)
    owner.deals.create! :table => @opt.sample, :home=> @opt.sample, :pick=> @opt.sample, :restaurant_name => owner.restaurant_name, :postcode => owner.postcode, :launched => @launched.sample, :location => owner.location, :type => owner.type, :menuitems => Menuitem.all.sample(3), :price => price
  end
end

@user = User.create! :name => "osman", :mobile_number => "03451111111", :email => "buyer@familyfeast.com", :password => "dealchef", :password_confirmation => "dealchef"

Deal.all.sample(3).each do |deal|
  @user.deals << deal
  end

Deal.each do |deal|
 5.times do
   deal.sales.create! :confirmation_code=> SecureRandom.hex(4).upcase, :uid=> @user.id
  end

end

