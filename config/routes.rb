Familyfeast::Application.routes.draw do
  match "/browseresdeals" => "home#browseresdeals"
  match "/browseresitems" => "home#browseresitems"
  match "/browserestaurants" => "home#browserestaurants"
  match "/salesstats" => "deals#salestats"
  get "home/buy"
  get "home/test"
  root :to => "home#index"
  devise_for :users
  devise_for :users do get '/user_signout' => 'devise/sessions#destroy' end
  resources :menuitems
  devise_for :owners
  devise_for :owners do get '/owner_signout' => 'devise/sessions#destroy' end
  resources :deals
  post "deals/create" => "deals#create"
  match  "newdeal/launch/:id" => "deals#launch"
  match "newdeal/launched"=> "deals#launched" , :via => :post
  match "/userdeals" => "home#userdeals"
  match "/order" => "home#order"
  match "/mid_edit" => "deals#mid_edit"
  match "/showdeals" => "home#showdeals"
  match "/restaurants" => "home#restaurants"
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
# match ':controller(/:action(/:id))(.:format)'
 
end

